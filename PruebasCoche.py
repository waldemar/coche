import unittest
from Coche import *

class TestCoche(unittest.TestCase):
    def test_creacion_coche(self):
        coche = Coche('rojo', 'Toyota', 'Corolla', 'ABC123', 30)
        self.assertEqual(coche.velocidad, 30)

    def test_acelerar(self):
        coche = Coche('rojo', 'Toyota', 'Corolla', 'ABC123', 30)
        coche.acelerar(20)
        self.assertEqual(coche.velocidad, 50)

    def test_frenar(self):
        coche = Coche('rojo', 'Toyota', 'Corolla', 'ABC123', 30)
        coche.frenar(10)
        self.assertEqual(coche.velocidad, 20)

    def test_obtener_total_coches(self):
        total_inicial = Coche.obtener_total_coches()
        coche1 = Coche.crear_coche('rojo', 'Toyota', 'Corolla', 'ABC123', 30)
        coche2 = Coche.crear_coche('azul', 'Ford', 'Focus', 'XYZ456', 40)
        total_despues = Coche.obtener_total_coches()
        self.assertEqual(total_despues - total_inicial, 2)

    def test_str(self):
        coche = Coche('rojo', 'Toyota', 'Corolla', 'ABC123', 30)
        self.assertEqual("Coche de color=rojo, marca=Toyota, modelo=Corolla, matricula=ABC123, velocidad=30)",coche.__str__())

if __name__ == "__main__":
 unittest.main()