class Coche:
    """Clase que representa un coche."""

    total_coches = 0  # Variable de clase para contabilizar el número total de coches

    def __init__(self, color, marca, modelo, matricula, velocidad):
        """
        Constructor de la clase Coche.

        :param color: Color del coche.
        :param marca: Marca del coche.
        :param modelo: Modelo del coche.
        :param matricula: Matrícula del coche.
        :param velocidad: Velocidad inicial del coche.
        """
        self._color = color
        self._marca = marca
        self._modelo = modelo
        self._matricula = matricula
        self._velocidad = velocidad
        Coche.total_coches += 1  # Incrementar el contador al crear una nueva instancia

    # Métodos getter y setter para el color
    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, nuevo_color):
        self._color = nuevo_color 

    # Métodos getter y setter para la marca
    @property
    def marca(self):
        return self._marca

    @marca.setter
    def marca(self, nueva_marca):
        self._marca = nueva_marca

    # Métodos getter y setter para el modelo
    @property
    def modelo(self):
        return self._modelo

    @modelo.setter
    def modelo(self, nuevo_modelo):
        self._modelo = nuevo_modelo 

    # Métodos getter y setter para la matrícula
    @property
    def matricula(self):
        return self._matricula

    @matricula.setter
    def matricula(self, nueva_matricula):
        self._matricula = nueva_matricula

    # Métodos getter y setter para la velocidad
    @property
    def velocidad(self):
        return self._velocidad 

    @velocidad.setter
    def velocidad(self, nueva_velocidad):
        self._velocidad = nueva_velocidad

    @classmethod
    def obtener_total_coches(cls):
        """
        Método de clase para obtener el número total de coches creados.
        """
        return cls.total_coches
    
    def acelerar(self, incremento):
        """Acelera el coche aumentando su velocidad."""
        self._velocidad += incremento

    def frenar(self, decremento):
        """Frena el coche reduciendo su velocidad."""
        if self._velocidad >= decremento:
            self._velocidad -= decremento
        else:
            self._velocidad = 0

    @classmethod
    def crear_coche(cls, color, marca, modelo, matricula, velocidad):
        """
        Método de clase que crea una nueva instancia de Coche.

        :param color: Color del coche.
        :param marca: Marca del coche.
        :param modelo: Modelo del coche.
        :param matricula: Matrícula del coche.
        :param velocidad: Velocidad inicial del coche.
        :return: Una nueva instancia de Coche.
        """
        return cls(color, marca, modelo, matricula, velocidad)

    # Métodos getter y setter para el color, marca, modelo, matrícula y velocidad (omito por brevedad)

    def __str__(self):
        """
        Método para obtener una representación en cadena del objeto Coche.
        """
        return f"Coche de color={self._color}, marca={self._marca}, modelo={self._modelo}, matricula={self._matricula}, velocidad={self._velocidad})"

# Ejemplo de uso
coche1 = Coche.crear_coche('rojo', 'Toyota', 'Corolla', 'ABC123', 30)
coche2 = Coche.crear_coche('azul', 'Ford', 'Focus', 'XYZ456', 40)

print("Total de coches:", Coche.obtener_total_coches())  # Salida: 2
print(coche1)  # Salida: Coche(color=rojo, marca=Toyota, modelo=Corolla, matricula=ABC123, velocidad=30)
print(coche2)  # Salida: Coche(color=azul, marca=Ford, modelo=Focus, matricula=XYZ456, velocidad=40)
print(coche1.velocidad)